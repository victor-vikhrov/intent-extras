package com.example.itschool.registerationexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_profile);

        Intent intent = getIntent();
        String login = intent.getExtras().getString("login");
        String password = intent.getExtras().getString("password");
        TextView text = new TextView(this);
        text.setText(login + "\n" + password);
        setContentView(text);
    }

}
